function countLetter(letter, sentence) {
    // Check if the letter is a single character
    if (letter.length !== 1) {
        return undefined;
    }

    let result = 0;

    // Count the number of times the letter appears in the sentence
    for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
            result++;
        }
    }

    // Check if invalidLetter is a single character
    if (letter === 'abc') {
        return undefined;
    }

    return result;


    // check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
}
console.log(countLetter('o', 'The quick brown fox jumps over the lazy dog')); 
console.log(countLetter('abc', 'The quick brown fox jumps over the lazy dog')); 

function isIsogram(text) {

      text = text.toLowerCase();

      const letters = {};

      for (let i = 0; i < text.length; i++) {
        const letter = text[i];
        
        if (letters[letter]) {
          return false;
        }
        letters[letter] = true;
      }

      return true;

    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.  
}

console.log(isIsogram("Machine"));
console.log(isIsogram("Hello"));


function purchase(age, price) {

     
    if (age < 13) {
    return undefined;

    }  else if (age >= 13 && age <= 21 || age >= 65) {
    
    discountedPrice = price * 0.8;
    } else {
    
    discountedPrice = Math.round(price * 100) / 100;

    }

    return discountedPrice.toFixed(2);

    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

}

console.log(purchase(12, 109.4356));
console.log(purchase(15, 109.4356));
console.log(purchase(65, 109.4356));


function findHotCategories(items) {


  const categories = new Set();
  
        items.forEach(item => {
        if (item.stocks === 0) {
            categories.add(item.category);
            }
      });

  return Array.from(categories);
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {


   
  const commonVoters = [];

  for (let i = 0; i < candidateA.length; i++) {

    if (candidateB.includes(candidateA[i])) {

      commonVoters.push(candidateA[i]);
    }
    
  }

  return commonVoters;


    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};